��    �      $  �   ,      �
     �
     �
               )     0     >  7   V  4   �  �  �     g  q   z     �     �  *        2     M  	   \     f     l     z  
   �     �     �     �  
   �     �     �     �          
          0  &   P     w  &   �      �      �  5   �     +     E     Y     h     �     �     �     �     �  
   �     �      �       �   &     �     �     �     �  	              8     L     Q     W     f     l     {     �     �     �  ,   �  !        -  �   @          '  �   ,     �     �  !        (     .     =     J  #   Z     ~     �  %   �     �     �     �                  
   4     ?  "   P     s     z  	   �     �     �     �     �     �  	   �     �     �  $     3   '     [     r     �  $   �     �     �  
   �  _   �     Q     Y  �   i       
             $     =     B  $   U  	   z  �  �     !     %!     A!     T!  	   t!     ~!     �!  9   �!  5   �!  �  "     �)  �   �)     W*     ^*  ,   p*     �*     �*      �*     �*     �*     �*     +  *   +     A+     G+     f+     s+     |+     �+  	   �+     �+     �+  .   �+  2   ,  "   C,  .   f,  ,   �,  &   �,  0   �,     -  #   9-     ]-     n-  '   �-     �-     �-     �-     �-     �-     �-     �-      .  {   4.  #   �.  *   �.     �.  $   /     9/  $   H/     m/     �/  	   �/     �/     �/     �/     �/     �/     �/  !   0  -   60  .   d0     �0    �0     �1     �1  �   �1     �2     �2  5   �2      3     3     3     /3  #   A3     e3  #   �3  *   �3     �3     �3     4     4  
   :4     E4     Y4     g4     �4  	   �4     �4  	   �4     �4  (   �4     5     &5     85     A5     N5     Z5  $   u5  O   �5      �5  	   6  &   6  7   <6     t6     �6     �6  }   �6     %7     -7  �   A7  	   �7     8     8  "   8     :8     C8  '   _8     �8     }   K         u   k       l      z   Z   U   f   1   b   [           ,   j   (   W         t   e           C   :   =   6   <   G                 I       y           H   "       ~      F             q   m           _   S   R   E   N   w       '          |   i         2   D          d   A           p       {   #   4   �      v   *   3   >   x       -   g   ;   �   !       
   8       @               )   c          o   J   %   .          M   L                      a              O      V   9       +   Y   5   /          ?      X      n   \   B   h   `       &          ]               P             T          s      $              7   	   0   Q   ^   r    %1$s at %2$s &larr; Older Comments &larr; Previous &larr; Previous post (Edit) (Post author) 1 Comment %1$s Comments <i class="icon_cart" aria-hidden="true"></i>GET PREMIUM <i class="icon_house" aria-hidden="true"></i>Welcome <strong>What the HappenStance Premium Version offers in addition?</strong><br />
    - Support for Drag-and-drop Page Builder with 37 in-built widgets for creating custom page layouts<br />
    - 8 pre-defined color schemes (Blue, Gray, Green, Orange, Pink, Purple, Red and Turquoise)<br />
    - Unlimited ability to create custom Color scheme (unlimited color settings)<br />
    - Ability to set different Header Images for the individual pages/posts/categories<br />
    - Ability to set different Header Slideshows for the individual pages/posts<br />
    - Ability to set different Sidebars/Footers for the individual pages/posts<br />
    - Image Slideshow with 3 different templates<br />
    - Font size settings<br />
    - Related Posts box on the single posts<br />
    - 4 dynamic Hover effects for the Post Thumbnails (Fade, Focus, Shadow and Tilt)<br />
    - 6 Custom widgets for displaying the latest posts from the specific categories (as a Column, Grid, List, Slider, Thumbnails and by Default - One Column)<br />
    - Custom Tab widget (displays popular posts, recent posts, comments and tags in tabbed format)<br />
    - Info-Box Custom widget (displays a text box with an icon)<br />
    - Header Carousel box (slider with your Latest Posts or a Custom Menu)<br />
    - Social Network Profile links in header (69 pre-defined icons)<br />
    - Facebook Like Box Custom widget<br />
    - Twitter Following Custom widget<br />
    - Integrated Facebook/Twitter/Google+1 share buttons on posts/pages/post entries<br />
    - Integrated automatic Sitemap generator with advanced options<br />
    - Custom Shortcodes for adding buttons, images, slideshows, tables and highlighted texts anywhere you like<br />
    - Custom Shortcode for displaying Google maps<br />
    - Custom Shortcode for displaying specific listing of posts anywhere you like<br />
    - 6 Custom Page templates<br />
    - Ability to add custom JavaScript code About HappenStance Apologies, but no results were found for your request. Perhaps searching will help you to find a related content. Archive Author Archive: %s Background Pattern (in Boxed layout style) Background Pattern Opacity Blue (default) Body font Boxed Character Set Color Scheme Comment... Comments are closed. Content Content/Excerpt Displaying Custom CSS Cyrillic Cyrillic Extended Daily Archive: %s Default Display Display Breadcrumb Navigation Display Featured Image on pages Display Featured Image on single posts Display Header Image Display Manual Excerpt on single posts Display Meta Box on Post Entries Display Meta Box on single posts Display Next/Previous Post Navigation on single posts Display Scroll-top Button Display Search Form Display Shadow Display Sidebar on Archives Display Sidebar on Posts/Pages Display Site Description Documentation Edit Email Address Everywhere Excerpt Excerpt Length (number of words) Featured Images Size First of all, I would like to thank you for choosing the HappenStance theme! I firmly believe that you will be satisfied with this template. Footer left widget area Footer middle widget area Footer notices Footer right widget area Full Size Get HappenStance Premium Version Get Premium Version Gray Greek Greek Extended Green Grid - Masonry HappenStance HappenStance Font Settings HappenStance General Settings HappenStance Header Settings HappenStance Post Entries/Blog Page Settings HappenStance Posts/Pages Settings HappenStance Theme HappenStance is an easily customizable theme which can be used for your Blog, Magazine, Business, eCommerce or Events website. It is a fully responsive and Retina ready theme that allows for easy viewing on any device. Header Logo Hide If you would like to purchase the HappenStance Premium Version, you can do so on <a href="http://www.tomastoman.cz/downloads/happenstance/" target="_blank">Developers Official Website</a>. Install Plugins Latest Posts Latest Posts (Blog) page headline Latin Latin Extended Layout Style Leave a Comment Left column with widgets in footer. Main Header Menu Main Header Menu font Middle column with widgets in footer. Monthly Archive: %s Newer Comments &rarr; Next &rarr; Next post &rarr; Nothing Found Number of Results:  One Column Only on Homepage Page/Post Headlines (h1 - h6) font Pages: Phone Number Pingback: Post Entries Format Post Entry Headline font Post navigation Postal Address Purple Read more Reply Right Sidebar Right column with widgets in footer. Right sidebar which appears on all posts and pages. Search Results for: %s Search here... Search results navigation Sidebar/Footer Widget Headlines font Site Description font Site Title font Skype Name Sorry, but nothing matched your search criteria. Please try again with some different keywords. Support Tag Archive: %s The line for copyright and other notices below the footer widget areas. Insert here one Text widget. The "Title" field at this widget should stay empty. Thumbnail Size Vietnamese Website Welcome to HappenStance! Wide Yearly Archive: %s Your comment is awaiting moderation. Your name Project-Id-Version: HappenStance 1.0
POT-Creation-Date: 2014-11-29 08:37+0100
PO-Revision-Date: 2016-06-30 20:08+0100
Last-Translator: 
Language-Team: Tomas Toman <webdesign@tomastoman.cz>
Language: Čeština
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n == 1 ? 0 : 1;
X-Generator: Poedit 1.5.7
X-Poedit-SourceCharset: UTF-8
 %1$s v %2$s &larr; Starší komentáře &larr; Předchozí &larr; Předchozí příspěvek (Upravit) (Autor) 1 Komentář %1$s Komentářů <i class="icon_cart" aria-hidden="true"></i>VERZE PREMIUM <i class="icon_house" aria-hidden="true"></i>Vítejte <strong>Co vše nabízí HappenStance Premium Verze?</strong><br />
    - Support for Drag-and-drop Page Builder with 37 in-built widgets for creating custom page layouts<br />
    - 8 pre-defined color schemes (Blue, Gray, Green, Orange, Pink, Purple, Red and Turquoise)<br />
    - Unlimited ability to create custom Color scheme (unlimited color settings)<br />
    - Ability to set different Header Images for the individual pages/posts/categories<br />
    - Ability to set different Header Slideshows for the individual pages/posts<br />
    - Ability to set different Sidebars/Footers for the individual pages/posts<br />
    - Image Slideshow with 3 different templates<br />
    - Font size settings<br />
    - Related Posts box on the single posts<br />
    - 4 dynamic Hover effects for the Post Thumbnails (Fade, Focus, Shadow and Tilt)<br />
    - 6 Custom widgets for displaying the latest posts from the specific categories (as a Column, Grid, List, Slider, Thumbnails and by Default - One Column)<br />
    - Custom Tab widget (displays popular posts, recent posts, comments and tags in tabbed format)<br />
    - Info-Box Custom widget (displays a text box with an icon)<br />
    - Header Carousel box (slider with your Latest Posts or a Custom Menu)<br />
    - Social Network Profile links in header (69 pre-defined icons)<br />
    - Facebook Like Box Custom widget<br />
    - Twitter Following Custom widget<br />
    - Integrated Facebook/Twitter/Google+1 share buttons on posts/pages/post entries<br />
    - Integrated automatic Sitemap generator with advanced options<br />
    - Custom Shortcodes for adding buttons, images, slideshows, tables and highlighted texts anywhere you like<br />
    - Custom Shortcode for displaying Google maps<br />
    - Custom Shortcode for displaying specific listing of posts anywhere you like<br />
    - 6 Custom Page templates<br />
    - Ability to add custom JavaScript code O Šabloně HappenStance Je nám líto, ale nebyly nalezeny žádné výsledky pro váš požadavek. Možná, že vyhledávání vám pomůže najít související obsah. Archiv Archiv autora: %s Vzorek na pozadí (pro Styl Rozvržení Box) Průhlednost Vzorku Modrá (výchozí) Písmo Hlavní Obsahové Části Box Znaková Sada Barevné Schéma Komentář... Přidávání komentářů není povoleno. Obsah Zobrazovat Celý Obsah/Úryvek Vlastní CSS Cyrilice Cyrilice Rozšířená Archiv článků ze dne: %s Výchozí Zobrazit Zobrazit Drobečkovou Navigaci Zobrazovat Náhledový Obrázek na Stránkách Zobrazovat Náhledový Obrázek na Příspěvcích Zobrazování Obrázku v Záhlaví Zobrazovat Stručný výpis na Příspěvcích Zobrazovat Meta Box ve Výpisu Příspěvků Zobrazovat Meta Box na Příspěvcích Navigace mezi Příspěvky (Předchozí/Další) Zobrazit Scroll-top Tlačítko Zobrazovat Vyhledávací Formulář Zobrazovat Stín Zobrazit na Archivech Zobrazit na Příspěvcích/Stránkách Zobrazovat Popis Webu Dokumentace Upravit Emailová Adresa Všude Úryvek Délka Úryvku (počet slov) Velikost Náhledových Obrázků Na úvod bych Vám chtěl poděkovat za instalaci šablony HappenStance! Pevně doufám, že budete se šablonou spokojeni. Levá oblast pro widgety v zápatí Prostřední oblast pro widgety v zápatí Informace v zápatí Pravá oblast pro widgety v zápatí Plná Velikost Získejte HappenStance Premium Verzi Získat Verzi Premium Šedá Řečtina Řečtina Rozšířená Zelená Mřížka - Masonry HappenStance HappenStance Nastavení Písma HappenStance Hlavní Nastavení HappenStance Nastavení Záhlaví HappenStance Nastavení Výpisu Příspěvků HappenStance Nastavení Příspěvků/Stránek Šablona HappenStance HappenStance je snadno přizpůsobitelná šablona, kterou můžete použít pro Váš blog, magazín, firemní stránky, e-shop či web s nabídkou pořádaných akcí. Šablona je plně responsivní, což umožňuje snadné prohlížení na všech zařízeních. Logo v Záhlaví Skrýt Pokud byste si přáli zakoupit HappenStance Premium Verzi, můžete tak učinit na <a href="http://www.tomastoman.cz/downloads/happenstance/" target="_blank">oficiálních stránkách jejího vývojáře</a>. Instalace Pluginů Nejnovější Příspěvky Nadpis Stránky s nejnovějšími Příspěvky (Blog) Latinka Latinka Rozšířená Styl Rozvržení Napsat komentář Levý sloupec s widgety v zápatí. Hlavní navigace v záhlaví Písmo Hlavní Navigace v Záhlaví Prostřední sloupec s widgety v zápatí. Archiv článků za měsíc: %s Novější komentáře &rarr; Další &rarr; Další příspěvek &rarr; Nenalezeno Počet výsledků:  Jeden Sloupec Pouze na domovské stránce Písmo Nadpisů (h1 - h6) Stránky: Telefonní Číslo Pingback: Formát Výpisu Příspěvků Písmo Nadpisů ve Výpisu Příspěvků Navigace mezi příspěvky Poštovní Adresa Fialová Číst více Odpovědět Hlavní postranní sloupec Pravý sloupec s widgety v zápatí. Pravý postranní sloupec, který se objeví u všech stránek a příspěvků. Výsledky vyhledávání pro: %s Hledat... Navigace mezi výsledky vyhledávání Písmo Nadpisů Widgetů v Postranním Sloupci/Zápatí Písmo Popisu Webu Písmo Názvu Webu Skype Jméno Je nám líto, ale Vašim kritériím nevyhovuje žádný výsledek. Zkuste to prosím znovu s jiným vyhledávacím dotazem. Podpora Archiv štítku: %s Řádek pro informace o autorství a další poznámky pod oblastmi pro widgety v zápatí. Vložte sem jeden textový widget. Pole "Název" by u tohoto widgetu mělo zůstat prázdné. Miniatura Vietnamština Web Vítá Vás Šablona HappenStance! Široký Archiv článků za rok: %s Váš komentář čeká na schválení. Vaše jméno 