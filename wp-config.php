<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
 
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tornadoinfra');

/** MySQL database username */
define('DB_USER', 'tornadoinfra');

/** MySQL database password */
define('DB_PASSWORD', 'P@ssw0rd1473');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(4xtY`prtJ56<Y3o2m,$^7OkhdiOTJx_;daB0npOGp_6iIbTv[:N< #58&6|s 4^');
define('SECURE_AUTH_KEY',  'G,(Ljm0FQiYarP1H}}Y ]WEY!}0Y#+hT}p;l_WSA[9UL@hKvC2uQj3%,^tF<?VS`');
define('LOGGED_IN_KEY',    'D(![1 3fQ%hid(&{DraORV,v+K?v-Y0}8O<6C:N.Qd^^b:%z=KP*uoL.E^Wj@%K*');
define('NONCE_KEY',        'n;we1W]&H{BG}dESv0-lDqq+- CaHBH0{#j}e*[F:q6&?Yg-H` At}8bb>gtFe68');
define('AUTH_SALT',        ']6@CcPoB{I~7Xnz~x|sJfwq~/{G&hA/S8[m!{v*8J&l8j(<L_6j+VOQi0*Or0&2m');
define('SECURE_AUTH_SALT', 'D:B]:Vz1,7sE#rQrn&QNJfZYHAM^+m>QI/M.?+a4WG/;h:~d-KVNXEC$zF@a:1iG');
define('LOGGED_IN_SALT',   'D[)XyWEc*0mk72gkto&:jNQN[fqe&mBgo36erMh}52=clL&WXSl&Qo[]IO%[{PQB');
define('NONCE_SALT',       'e*m%J95 4)ydgo[YDV JlOQn1w#nadI-~(,>{n/]H+}p3r~j==8:8%JGnAa- t_3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');